from __future__ import division, print_function, unicode_literals
import argparse
import h5py
import numpy as np
import tensorflow as tf
import sys
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from loss import spread_loss, cross_entropy, margin_loss
from network import baseline_model_kimcnn, baseline_model_cnn, capsule_model_A, capsule_model_B
from sklearn.utils import shuffle

tf.reset_default_graph()
np.random.seed(0)
tf.set_random_seed(0)

parser = argparse.ArgumentParser()

parser.add_argument('--embedding_type', type=str, default='static',
                    help='Options: rand (randomly initialized word embeddings), static (pre-trained embeddings from word2vec, static during learning), nonstatic (pre-trained embeddings, tuned during learning), multichannel (two embedding channels, one static and one nonstatic)')

parser.add_argument('--dataset', type=str, default='reuters_multilabel_dataset',
                    help='Options: reuters_multilabel_dataset, MR_dataset, SST_dataset')

parser.add_argument('--loss_type', type=str, default='margin_loss',
                    help='margin_loss, spread_loss, cross_entropy')

parser.add_argument('--model_type', type=str, default='capsule-B',
                    help='CNN, KIMCNN, capsule-A, capsule-B')

parser.add_argument('--has_test', type=int, default=1, help='If data has test, we use it. Otherwise, we use CV on folds')    
parser.add_argument('--has_dev', type=int, default=1, help='If data has dev, we use it, otherwise we split from train')    

parser.add_argument('--num_epochs', type=int, default=20, help='Number of training epochs')
parser.add_argument('--batch_size', type=int, default=32, help='Batch size for training')

parser.add_argument('--use_orphan', type=bool, default='True', help='Add orphan capsule or not')
parser.add_argument('--use_leaky', type=bool, default='False', help='Use leaky-softmax or not')
parser.add_argument('--learning_rate', type=float, default=0.001, help='learning rate for training')#CNN 0.0005 
parser.add_argument('--margin', type=float, default=0.2, help='the initial value for spread loss')
parser.add_argument('--dtype', type=str, default='float32', help='type of tensors')
import json
args = parser.parse_args()
params = vars(args)
print(json.dumps(params, indent = 2))

class BatchGenerator(object):
    """Generate and hold batches."""
    def __init__(self, dataset,label, batch_size, is_shuffle=True):
      self._dataset = dataset
      self._label = label
      self._batch_size = batch_size    
      self._cursor = 0       
      
      self._n_samples = int(np.shape(self._dataset)[0])
      if is_shuffle:
          index = np.arange(self._n_samples)
          np.random.shuffle(index)
          self._dataset = self._dataset
          self._dataset = np.array(self._dataset)[index]
          self._label = np.array(self._label)[index]
      else:
          self._dataset = np.array(self._dataset)
          self._label = np.array(self._label)

    # Override next func
    def next(self):
      if self._cursor + self._batch_size > self._n_samples:
          self._cursor = 0
      """Generate a single batch from the current cursor position in the data."""
      batch_x = self._dataset[self._cursor : self._cursor + self._batch_size,:]
      batch_y = self._label[self._cursor : self._cursor + self._batch_size]
      self._cursor += self._batch_size
      return batch_x, batch_y

# Load data
import pickle
with open('train_data_top_10_remap', 'rb') as f:
	train_data = pickle.load(f)
with open('test_data_top_10_remap', 'rb') as f:
	test_data = pickle.load(f)

train = train_data['data']
train_label = train_data['labels']
test = test_data['data']
test_label = test_data['labels']
args.num_classes = 90

del train[-1]
del train_label[-1]

train = np.reshape(np.array(train), newshape=(-1, args.batch_size, 768))
train = train.tolist()
X_embedding = tf.Variable(np.array(train), trainable=False, expected_shape=np.shape(train), dtype=args.dtype)

X_embedding = tf.transpose(X_embedding, perm=[1,0,2])
X_embedding = tf.expand_dims(X_embedding, axis=3)
train = tf.reshape(np.array(train), [8256, -1])
with tf.Session() as sess:
	train = train.eval(session=sess)


with tf.device('/cpu:0'):
    global_step = tf.train.get_or_create_global_step()

args.max_sent = 768
threshold = 0.5

X = tf.placeholder(tf.int32, [args.batch_size, 768], name="input_x")
y = tf.placeholder(tf.int64, [args.batch_size, args.num_classes], name="input_y")
is_training = tf.placeholder_with_default(False, shape=())    
learning_rate = tf.placeholder(dtype=args.dtype)
margin = tf.placeholder(shape=(),dtype=args.dtype) 

l2_loss = tf.constant(0.0)

tf.logging.info("input dimension:{}".format(X_embedding.get_shape()))

if args.model_type == 'capsule-A':
    poses, activations = capsule_model_A(X_embedding, args.num_classes)
if args.model_type == 'capsule-B':    
    poses, activations = capsule_model_B(X_embedding, args.num_classes)    
if args.model_type == 'CNN':    
    poses, activations = baseline_model_cnn(X_embedding, args.num_classes)
if args.model_type == 'KIMCNN':    
    poses, activations = baseline_model_kimcnn(X_embedding, args.max_sent, args.num_classes)   
    
if args.loss_type == 'spread_loss':
    loss = spread_loss(y, activations, margin)
if args.loss_type == 'margin_loss':    
    loss = margin_loss(y, activations)
if args.loss_type == 'cross_entropy':
    loss = cross_entropy(y, activations)

y_pred = tf.argmax(activations, axis=1, name="y_proba")    
correct = tf.equal(tf.argmax(y, axis=1), y_pred, name="correct")
accuracy = tf.reduce_mean(tf.cast(correct, tf.float32), name="accuracy")

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)   
training_op = optimizer.minimize(loss, name="training_op")
gradients, variables = zip(*optimizer.compute_gradients(loss))

grad_check = [tf.check_numerics(g, message='Gradient NaN Found!')
              for g in gradients if g is not None] + [tf.check_numerics(loss, message='Loss NaN Found')]
with tf.control_dependencies(grad_check):
    training_op = optimizer.apply_gradients(zip(gradients, variables), global_step=global_step)      

sess = tf.InteractiveSession()
from keras import utils

n_iterations_per_epoch = np.shape(train)[0] // args.batch_size
n_iterations_test = len(test) // args.batch_size

mr_train = BatchGenerator(train, train_label, args.batch_size)
mr_test = BatchGenerator(test, test_label, args.batch_size, is_shuffle=False)

best_model = None
best_epoch = 0
best_acc_val = 0.

init = tf.global_variables_initializer()
sess.run(init)     

lr = args.learning_rate
m = args.margin


for epoch in range(args.num_epochs):
    for iteration in range(1, n_iterations_per_epoch + 1):
        print('Iter ', iteration)
        X_batch, y_batch = mr_train.next()   
        y_batch = utils.to_categorical(y_batch, args.num_classes)
        '''
        This is what causes crash
        ################################'''
        _, loss_train, probs, capsule_pose = sess.run(
            [training_op, loss, activations, poses],
            feed_dict={X: X_batch[:,:args.max_sent],
                       y: y_batch,
                       is_training: True,
                       learning_rate:lr,
                       margin:m})
        '''##############################'''
        print('And till here')
        print("\rIteration: {}/{} ({:.1f}%)  Loss: {:.5f}".format(
                  iteration, n_iterations_per_epoch,
                  iteration * 100 / n_iterations_per_epoch,
                  loss_train),
              end="")
               
    preds_list, y_list = [], []
    for iteration in range(1, n_iterations_test + 1):
        X_batch, y_batch = mr_test.next()             
        probs = sess.run([activations],
                feed_dict={X:X_batch[:,:args.max_sent],
                           is_training: False})
        preds_list = preds_list + probs[0].tolist()
        y_list = y_list + y_batch.tolist()
        
    y_list = np.array(y_list)
    preds_probs = np.array(preds_list)                
    preds_probs[np.where( preds_probs >= threshold )] = 1.0
    preds_probs[np.where( preds_probs < threshold )] = 0.0 
    
    [precision, recall, F1, support] = \
        precision_recall_fscore_support(y_list, preds_probs, average='samples')
    acc = accuracy_score(y_list, preds_probs)

    print ('\rER: %.3f' % acc, 'Precision: %.3f' % precision, 'Recall: %.3f' % recall, 'F1: %.3f' % F1)  
    if args.model_type == 'CNN' or args.model_type == 'KIMCNN':
        lr = max(1e-6, lr * 0.8)
    if args.loss_type == 'margin_loss':    
        m = min(0.9, m + 0.1)
