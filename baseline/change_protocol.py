import pickle

with open('train_data', 'rb') as f:
	train_data = pickle.load(f)

with open('test_data', 'rb') as f:
	test_data = pickle.load(f)

pickle.dump(train_data, open('train_data_top_10_remap', 'wb'), protocol=2)
pickle.dump(test_data, open('test_data_top_10_remap', 'wb'), protocol=2)