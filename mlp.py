import numpy as np
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.functional as F
from torchvision import datasets, transforms
import argparse
import subprocess
import time
import os
import datetime
import pickle
import torch.optim as optim
from pytorch_pretrained_bert import BertTokenizer
from torch.nn import Conv1d, MaxPool1d, Linear, ReLU
from dataset_loading import return_dataloader
from torch.utils.data import DataLoader, Dataset
from dataset_loading import reuters_dataset
from pytorch_pretrained_bert import BertModel

class MLP(torch.nn.Module):

  def __init__(self, in_dim, layer_dims, num_classes):
    super(MLP, self).__init__()

    if layer_dims is None:
      self.model = nn.Linear(in_dim, num_classes)
      return

    input_layer = nn.Linear(in_dim, layer_dims[0])
    self.layers = [input_layer, nn.ReLU()]

    for idx in range(len(layer_dims)-1):
      self.layers.extend([nn.Linear(layer_dims[idx], layer_dims[idx+1]),
                          nn.ReLU()
                          ])

    output_layer = nn.Linear(layer_dims[-1], num_classes)
    self.layers.append(output_layer)
    self.model = nn.Sequential(*self.layers)

  def forward(self, x):
    # Flatten
    x = x.view(x.size(0), -1)
    # Pass to linear layers
    x = self.model(x)

    return x

if __name__ == "__main__":
  model = MLP(768, [200,300,400], 10)
  model = MLP(768, None, 10)
  model = MLP(768, [100], 10)
  print(model)