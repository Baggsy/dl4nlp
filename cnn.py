import numpy as np
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.functional as F
from torchvision import datasets, transforms
import argparse
import subprocess
import time
import os
import datetime
import pickle
import torch.optim as optim
from pytorch_pretrained_bert import BertTokenizer
from torch.nn import Conv1d, MaxPool1d, Linear, ReLU, Dropout
from dataset_loading import return_dataloader
from torch.utils.data import DataLoader, Dataset
from dataset_loading import reuters_dataset
from pytorch_pretrained_bert import BertModel

# Utility function to calc output size
def output_size(in_size, kernel_size, stride, padding):
  output = int((in_size - kernel_size + 2*(padding)) / stride) + 1
  return output//2

class CNN(torch.nn.Module):

  def __init__(self, in_dim, conv_channels, num_classes):
    super(CNN, self).__init__()

    self.in_dim = in_dim
    self.conv_channels = []

    for i in range(len(conv_channels)-1):
      in_channels = conv_channels[i]
      out_channels = conv_channels[i+1]
      self.conv_channels.extend([
                            Conv1d(in_channels, out_channels, kernel_size=3, stride=1, padding=1, bias=True),
                            MaxPool1d(kernel_size=2, stride=2, padding=0),
                            ReLU()
      ])
    self.conv_channels = nn.Sequential(*self.conv_channels)

    # Calc output size after processing by conv channels
    for i in range(len(conv_channels)-1):
      in_dim = output_size(in_dim, kernel_size=3, stride=1, padding=1)


    self.conv_out_size = ((in_dim)*conv_channels[-1])

    self.linears = nn.Sequential(nn.Linear(self.conv_out_size, out_features=256, bias=True),
                                 nn.ReLU(),
                                 nn.Dropout(0.5),
                                 nn.Linear(in_features=256, out_features=num_classes)
                                 )
  
  def forward(self, x):
    # Pass through conv channels
    x = self.conv_channels(x)
    # Flatten
    x = x.view(x.size(0), -1)
    # Pass to linear layers
    x = self.linears(x)

    return x
