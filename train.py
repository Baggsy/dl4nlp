import argparse
import subprocess
import time
import os
import datetime
import pickle
import torch
import numpy as np
import torch.optim as optim
import torch.nn as nn
from pytorch_pretrained_bert import BertTokenizer
from dataset_loading import return_dataloader
from cnn import CNN
from mlp import MLP
from kim_cnn import KimCNN
from utils import init_experiment, timeit, get_time
from torch.utils.tensorboard import SummaryWriter
import matplotlib.pyplot as plt


def get_class_weights(train_dataloader):
    targets_all = torch.Tensor([]).long()

    for batch_idx, batch in enumerate(train_dataloader):
        data, target = zip(*batch)
        data = torch.stack(data).float().to('cpu')
        target = torch.stack(target).long().to('cpu')
        targets_all = torch.cat((targets_all, target), 0)

    unique, counts = targets_all.unique(return_counts=True)
    class_weights = 1. / counts.float()
    samples_weights = [class_weights[t] for t in targets_all]

    with open('samples_weights.pkl', 'wb') as f:
        pickle.dump(samples_weights, f)
    with open('class_weights.pkl', 'wb') as f:
        pickle.dump(class_weights, f)
    
    return class_weights, samples_weights

def initialize_model(config):
    if config['model'] == 'CNN':
        model = CNN(in_dim = 768, conv_channels = config['conv_channels'], num_classes = config['top_k']+1)
    elif config['model'] == 'KimCNN':
        model = KimCNN(in_dim = 768, conv_channels = config['conv_channels'], num_classes = config['top_k']+1)
    elif config['model'] == 'CapsNet':
        pass
    elif config['model'] == 'MLP':
        model = MLP(in_dim = 768, layer_dims = config['mlp_linears'], num_classes = config['top_k']+1)

    optimizer = optim.Adam(model.parameters(), lr=config['lr'])

    return model, optimizer

def write_stats(writer, epoch, train_loss, train_acc, test_loss, test_acc):
    writer.add_scalar('train loss', train_loss, epoch)
    writer.add_scalar('train acc', train_acc, epoch)
    writer.add_scalar('test loss', test_loss, epoch)
    writer.add_scalar('test acc', test_acc, epoch)

def plot_stats(num_epochs, train_loss, train_acc, test_loss, test_acc):
    plt.grid()

    plt.subplot(1, 2, 1)
    plt.title('Accuracy')
    plt.plot(train_acc, label='train')
    plt.plot(test_acc, label='test')
    plt.legend()

    plt.subplot(1, 2, 2)
    plt.title('Loss')
    plt.plot(train_loss, label='train')
    plt.plot(test_loss, label='test')
    plt.legend()
    
    plt.show()



@timeit
def train_epoch(epoch, train_dataloader, model, optimizer, criterion, config, device):
    print('='*30)
    print('Epoch {}'.format(epoch))
    print('='*30)
    model.train()
    epoch_avg_loss = 0
    epoch_avg_acc = 0

    for batch_idx, batch in enumerate(train_dataloader):
        data, target = zip(*batch)
        data = torch.stack(data).float().to(device)
        target = torch.stack(target).long().to(device)
        
        data = data.unsqueeze(1)
        correct = 0

        optimizer.zero_grad()
        output = model(data).squeeze(1).float()
        target = target.view(-1)

        loss = criterion(output, target)
        
        loss.backward()
        optimizer.step()

        epoch_avg_loss += loss.item()
        
        pred = output.argmax(dim=1, keepdim=True)
        correct = pred.eq(target.view_as(pred)).sum().item()

        epoch_avg_acc += correct/config['batch_size']

    epoch_avg_acc /= batch_idx
    epoch_avg_loss /= batch_idx

    return epoch_avg_loss, epoch_avg_acc



def test_epoch(model, device, config, test_dataloader, criterion):
    model.eval()
    test_loss = 0
    test_acc = 0
    counter = 0

    with torch.no_grad():
        for batch_idx, batch in enumerate(test_dataloader):
          data, target = zip(*batch)
          data = torch.stack(data).float().to(device)
          target = torch.stack(target).long().to(device)
          data = data.unsqueeze(1)
          
          data, target = data.to(device), target.to(device)
          output = model(data).squeeze(1).float()
          target = target.view(-1).long()

          test_loss += criterion(output, target).item()

          pred = output.argmax(dim=1, keepdim=True).view_as(target) # get the index of the max log-probability
          correct = pred.eq(target).sum().item()

          # print(np.shape(target))
          # print(np.shape(pred))

          test_acc += correct/config['batch_size']
          counter += 1
    print('Target :', target)
    print('Prediction :', pred)
    test_loss /= counter
    test_acc /= counter          
    return test_loss, test_acc

@timeit
def train(config):
    # Set random seed + Initialise device

    summary_writer = SummaryWriter(config['tb_log_file']+'/'+get_time())

    train_losses, train_accs = [], []
    test_losses, test_accs = [], []
    init_experiment(config)

    config['tokeniser'] = BertTokenizer.from_pretrained('bert-base-uncased')
    train_dataloader = return_dataloader(config, mode='train')
    test_dataloader = return_dataloader(config, mode='test')


    device = "cuda" if torch.cuda.is_available() else "cpu"
    # Initialise model
    model, optimizer = initialize_model(config)
    model = model.to(device)
    criterion = nn.CrossEntropyLoss()#weight=config['weights'].to(device)

    # Iterate over epochs
    for epoch in range(1, config['epochs']+1):
        train_loss, train_acc = train_epoch(epoch, train_dataloader, model, optimizer, criterion, config, device=device)
        test_loss, test_acc = test_epoch(model, device, config, test_dataloader, criterion)
        
        train_accs.append(train_acc)
        test_accs.append(test_acc)

        train_losses.append(train_loss)
        test_losses.append(test_loss)

        write_stats(summary_writer, epoch, train_loss, train_acc, test_loss, test_acc)

        print('Train loss: {:>12} Train acc: {:>12}'.format(train_loss, train_acc))
        print('Test loss:  {:>12} Test acc:  {:>12}'.format(test_loss, test_acc))

    # Commented this line out cuz it will throw error if cuda is not available
    # print("Maximum memory cached for the whole duration of the experiment: {}".format(torch.cuda.max_memory_cached()))
    plot_stats(config['epochs'], train_losses, train_accs, test_losses, test_accs)

def main():
    parser = argparse.ArgumentParser()
    # Global
    parser.add_argument("--model", type=str, default="CNN", choices=['CNN', 'CapsNet', 'MLP', 'KimCNN'])
    parser.add_argument("--train_data_dir", type=str, default="training/")
    parser.add_argument("--test_data_dir", type=str, default="test/")
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--lr", type=float, default=1e-2)
    parser.add_argument("--epochs", type=int, default=40)
    parser.add_argument("--max_num_tokens", type=int, default=512)
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--resume", type=str, help="e.g.: 'results/exp_1/best_model'")
    parser.add_argument("--log_filename", type=str, default='output.log')
    parser.add_argument("--results_dir", type=str, default='results')
    parser.add_argument("--save_model_to_file", type=str, default=None, 
                        help='which file to save nn model to after training')
    parser.add_argument("--tb_log_file", type=str, default='tb_logs',
                        help='where to store tensorboard log')

    # Dataloading specific
    parser.add_argument("--train_load_from_file", type=str)
    parser.add_argument("--train_save_to_file", type=str, help='e.g.: train_data')
    parser.add_argument("--test_load_from_file", type=str)
    parser.add_argument("--test_save_to_file", type=str, help='e.g.: test_data')
    parser.add_argument("--labels_file_path", type=str, default='cats.txt')
    parser.add_argument("--batch_size_for_BERT_embeddings", type=int, default=32)
    parser.add_argument("-no_BERT_embeddings", dest="get_BERT_embeddings", action="store_false")
    parser.add_argument("-dont_pad", dest="do_padding", action="store_false")
    parser.add_argument("-best_match_paragraph", dest="best_match_paragraph", action="store_true")
    parser.add_argument("--top_k", type=int, default=-1)
    parser.add_argument('--splits', type=int)

    # CNN params
    parser.add_argument("--conv_channels", default=[1,100,100], 
                        help="list of number of conv. filters per layer for cnn", nargs='+', type=int)
    # MLP params
    parser.add_argument("--mlp_linears", default=[200,100], 
                        help="list of neurons per linear layer", nargs='+', type=int)
    # For debugging
    parser.add_argument("-d", dest="debug", action="store_true")
    parser.add_argument("-cpu", dest="cpu", action="store_true")
    parser.add_argument("--verbosity", type=int, default=1)
    parser.add_argument("-t", dest="temp", action="store_true")

    args = parser.parse_args()
    t0 = time.time()

    # train_dataloader = return_dataloader(vars(args), mode='train')
    # class_weights, _ = get_class_weights(train_dataloader)

    config = vars(args)
    # config['weights'] = class_weights
    train(config)


    print("\n-------------- Done in {0:.4f} [hours]---------------".format((time.time() - t0)/(60*60)))


if __name__ == '__main__':
    main()
