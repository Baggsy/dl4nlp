import pickle
import torch

with open('train_data_top_10', 'rb') as f:
	train_data = pickle.load(f)

with open('test_data_top_10', 'rb') as f:
	test_data = pickle.load(f)

# Get all the labels 
labels = torch.Tensor(train_data['labels']).unique().tolist()
# Create mapping
map_to_index = {int(key):value for (key,value) in zip(labels, range(11))}

# Apply mapping
for idx,elem in enumerate(train_data['labels']):
	train_data['labels'][idx] = map_to_index[elem]

for idx,elem in enumerate(test_data['labels']):
	test_data['labels'][idx] = map_to_index[elem]

# Overwrite files
with open('train_data_top_10_remap', 'wb') as f:
	pickle.dump(train_data, f)

with open('test_data_top_10_remap', 'wb') as f:
	pickle.dump(test_data, f)