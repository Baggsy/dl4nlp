import time
import random
import numpy as np
from torch.backends import cudnn
from torch.distributions.categorical import *
import os
import datetime
import logging
import sys
import re
from early_stopping import EarlyStopping
import matplotlib.pyplot as plt
import json
import pickle


def get_time():
    return datetime.datetime.now().strftime('%d-%m_%H:%M')


def timeit(method):
    def timed(*args, **kw):
        t1 = time.time()
        result = method(*args, **kw)
        t2 = time.time()
        print('Method %s took %f sec to execute.' % (method.__name__, t2-t1))
        return result
    return timed


def init_experiment(config):
    exp_info = dict()

    # Adjust for debugging
    if config['debug']:
        exp_info['batch_size'] = 2
        exp_info['batch_size_for_BERT_embeddings'] = 2

    # Random seeds and devices
    set_seed(config['seed'])
    if torch.cuda.is_available() and not config['cpu']:
        exp_info['cpu'] = False
        exp_info['device'] = torch.device('cuda')
        if config['verbosity'] >= 2:
            print(torch.__version__)
            print(torch.version.cuda)
            print(cudnn.version())
    else:
        exp_info['cpu'] = True
        exp_info['device'] = torch.device('cpu')

    # Experiment naming
    if config['resume'] is None:
        exp_info['starting_epoch'] = 1
        exp_info['train_losses'] = []
        exp_info['test_losses'] = []
        exp_info['train_metrics_list'] = []
        exp_info['test_metrics_list'] = []
        exp_info['checkpoint'] = None
        exp_info['exp_dir'] = os.path.join(config['results_dir'], config['model'])
        if config['temp']:
            exp_name = "Exp_temp"
        else:
            if not os.path.isdir(exp_info['exp_dir']) or len(os.listdir(exp_info['exp_dir'])) == 0 \
                    or os.listdir(exp_info['exp_dir']) == ['Exp_temp']:
                exp_num = 1
            else:
                latest_exp_name = sorted([a for a in os.listdir(exp_info['exp_dir']) if not a == 'Exp_temp'],
                                         key=natural_keys)[-1]
                exp_num = int(latest_exp_name.split('_')[1]) + 1
            exp_name = "Exp_{}_{}".format(exp_num, datetime.date.today())

        exp_info['exp_dir'] = os.path.join(exp_info['exp_dir'], exp_name)
        os.makedirs(exp_info['exp_dir'], exist_ok=True)
        exp_info['log'] = return_logger(os.path.join(exp_info['exp_dir'], config['log_filename']))
        exp_info['log'].info('Initialised: {}'.format(exp_info['exp_dir']))
    else:
        raise NotImplementedError

    # Early stopping
    exp_info['early_stopping'] = EarlyStopping(mode=config['early_stopping_mode'], min_delta=config['early_stopping_delta'],
                                              patience=config['early_stopping_patience'])

    config.update(exp_info)
    config['log'].info(config)
    print_exp_info(config)


def log_update_experiment_info(config, epoch_info, epoch):
    log_str = '[{}/{}] | {:4f}, {:4f} | {:4f}, {:4f} | {:4f}, {:4f}'.format(epoch, config['epochs'],
                                                                                     epoch_info['train_loss'],
                                                                                     epoch_info['test_loss'],
                                                                                     epoch_info['train_metrics']['f1'],
                                                                                     epoch_info['test_metrics']['f1'],
                                                                                     epoch_info['train_metrics']['acc'],
                                                                                     epoch_info['test_metrics']['acc'])
    config['log'].info(log_str)
    config['train_losses'].append(epoch_info['train_loss'])
    config['test_losses'].append(epoch_info['test_loss'])
    config['train_metrics_list'].append(epoch_info['train_metrics'])
    config['test_metrics_list'].append(epoch_info['test_metrics'])

    if epoch_info['train_metrics']['f1'] > epoch_info['max_score']:
        epoch_info['max_score'] = epoch_info['train_metrics']['f1']
        epoch_info['max_score_test'] = epoch_info['test_metrics']['f1']
        epoch_info['max_score_epoch'] = epoch
        epoch_info['max_score_epoch_info'] = log_str


def save_plots_param(config, epoch_info):
    def _dump_json(list_of_floats):
        if isinstance(list_of_floats[0], dict):
            return [{k: json.dumps(float(v)) for k,v in epoch.items()} for epoch in list_of_floats]
        return [json.dumps(float(a)) for a in list_of_floats]

    def _retrieve_metrics(list_of_dicts):
        temp_dict = dict()
        for metric in list_of_dicts[0].keys():
            temp_dict[metric] = [v[metric] for v in list_of_dicts]
        return temp_dict

    config['log'].info("\n -------------- \n Best score of {}, at epoch {} for exp ({}) \n   with info: {}\n -------------- ".format(
        config['test_metrics_list'][epoch_info['max_score_epoch']-1],
        epoch_info['max_score_epoch'],
        config['exp_dir'],
        epoch_info['max_score_epoch_info']))

    file_name_losses = os.path.join(config['exp_dir'], 'losses.json')
    config['log'].info('Saving losses info at {}'.format(file_name_losses))
    temp_dict = {'train_losses': _dump_json(config['train_losses']), 'test_losses': _dump_json(config['test_losses'])}
    json.dump(temp_dict, open(file_name_losses, 'w'))

    file_name_metrics = os.path.join(config['exp_dir'], 'metrics')
    config['log'].info('Saving metrics lists info at {}'.format(file_name_metrics))
    pickle.dump(config['train_metrics_list'], open(file_name_metrics+"train_pickle", 'wb'))
    pickle.dump(config['test_metrics_list'], open(file_name_metrics+"test_pickle", 'wb'))

    plt.plot(config['train_losses'])
    plt.plot(config['test_losses'])
    plt.xlabel('Epochs')
    plt.legend(['Train loss', 'Test loss'])
    plt.grid()
    plt.savefig(os.path.join(config['exp_dir'], 'losses.png'))
    plt.close()

    metrics_train = _retrieve_metrics(config['train_metrics_list'])
    metrics_test = _retrieve_metrics(config['test_metrics_list'])
    for metric in metrics_train:
        plt.plot(metrics_train[metric])
        plt.plot(metrics_test[metric])
        plt.xlabel('Epochs')
        plt.legend(['Train {}'.format(metric), 'Test {}'.format(metric)])
        plt.grid()
        plt.savefig(os.path.join(config['exp_dir'], '{}.png'.format(metric)))
        plt.close()


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
        cudnn.enabled = True
        cudnn.benchmark = True
        cudnn.deterministic = True


def natural_keys(text):
    """
    Returns the key list for float numbers in file names for more proper sorting
    """

    def atof(text):
        """
        Used for sorting numbers, in float format
        """
        try:
            retval = float(text)
        except ValueError:
            retval = text
        return retval

    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]


def return_logger(log_file):
    """
    Creates the logger
    """
    log = logging.getLogger(__name__)
    log.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    log.addHandler(fh)
    log.addHandler(ch)

    return log


def print_exp_info(config):
    str_log =  \
        "\n================================\n"\
        "Running {} from epoch {}:\n"\
        "   Max num of epochs: {}\n"\
        "   Learning rate: {}\n" \
        "   batch_size: {}\n"\
        "   num_classes: {}\n" \
        "   weighted_loss: {}\n"\
        "   bayesian_dropout: {}\n"\
        "   penalise_loss_with_std: {}\n"\
        "   bayesian_dropout_prob: {}\n"\
        "   get_BERT_embeddings: {}\n"\
        "   get_word_count_emb: {}\n"\
        "   token_embeddings: {}\n"\
        "   do_padding: {}\n"\
        "   best_match_paragraph: {}\n"\
        "   top_k: {}\n"\
        "   word_count_emb_max_words: {}\n"\
        "   do_early_stopping: {}\n"\
        "   Device: {}\n"\
        "   Seed: {}\n"\
        "   Debugging mode: {}\n"\
        "================================".format(
            config['model'], config['starting_epoch'],
            config['epochs'],
            config['lr'],
            config['batch_size'],
            config['num_classes'],
            config['weighted_loss'],
            config['bayesian_dropout'],
            config['penalise_loss_with_std'],
            config['bayesian_dropout_prob'],
            config['get_BERT_embeddings'],
            config['get_word_count_emb'],
            config['token_embeddings'],
            config['do_padding'],
            config['best_match_paragraph'],
            config['top_k'],
            config['word_count_emb_max_words'],
            config['do_early_stopping'],
            config['device'],
            config['seed'],
            config['debug']
           )
    # str_log = "\n================================\nRunning {} from epoch {}:\n"
    # for k, v in config.items():
    #     str_log += "    {}: {}\n".format(k, v)
    # str_log += "================================"

    config['log'].info(str_log)
