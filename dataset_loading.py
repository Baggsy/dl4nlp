import os
import numpy as np
import torch
import tqdm
import more_itertools as mit
import pickle
import collections

from pytorch_pretrained_bert import BertTokenizer
from torch.utils.data import DataLoader, Dataset
from keras.preprocessing.text import Tokenizer as keraTokenizer
from pytorch_transformers import BertForSequenceClassification

class reuters_dataset(Dataset):
    def __init__(self, config, args):
        """
        path is the directory with all the text document files
        """
        self.mode = args['mode']

        config['tokeniser'] = BertTokenizer.from_pretrained('bert-base-uncased')
        if args['load_from_file'] is None:
            print("Creating data from directory '{}' using labels file: {}".format(args['data_dir'],
                                                                                        config['labels_file_path']))
            self.data = self.load_data(args['data_dir'], config['labels_file_path'], config['tokeniser'],
                                       config['max_num_tokens'], config['get_BERT_embeddings'],
                                       config['batch_size_for_BERT_embeddings'],
                                       token_embeddings=config['token_embeddings'],
                                       doc_count_break_point=1 if config['debug'] else None,
                                       do_padding=config['do_padding'],
                                       best_match_paragraph=config['best_match_paragraph'],
                                       top_k=config['top_k'], train_dataset=args['train_dataset'],
                                       device=config['device'],
                                       get_word_count_emb=config['get_word_count_emb'],
                                       word_count_emb_max_words=config['word_count_emb_max_words'])
            
        else:
            print("Loading pre-saved data from {}".format(args['load_from_file']))
            self.data = pickle.load(open(args['load_from_file'], "rb"))

        self.require_grad = False
        if config['correct_label_matching']:
            self.correct_label_matching(train_dataset=args['train_dataset'])

        if not args['save_to_file'] is None:
            print("Saving data to '{}'".format(args['save_to_file']))
            if config['splits'] is not None:
                os.makedirs(config['save_splits_dir'], exist_ok=True)
                self.save_in_splits(config, args)
            else:
                pickle.dump(self.data, open(args['save_to_file'], 'wb'))

    def save_in_splits(self, config, args):
        split_range = int(np.ceil(1.0 * len(self.data['data']) / config['splits']))
        for split in range(config['splits']):
            pickle.dump(self.data['data'][split * split_range:min((split + 1) * split_range, len(self.data))],
                        open(os.path.join(config['save_splits_dir'], args['save_to_file'] + "_emb_" + str(split)), 'wb'))
        pickle.dump(self.data['labels'], open(os.path.join(config['save_splits_dir'], args['save_to_file'] + "_split_emb_labels"), 'wb'))

    def get_class_weights(self):
        label_counts = dict()
        for label in self.data['labels']:
            if label not in label_counts:
                label_counts[label] = 1.0
            else:
                label_counts[label] += 1.0
        weights = [1.0/v for v in label_counts.values()]
        # print(label_counts.values()) # single appearances
        # print(self.data['numbered_labeled_index_inverse'])
        # print(self.data['labels_index_dict'])
        # print({k:len(v) for k,v in self.data['labels_inverse_dict'].items()}) # total appearances.
        #     But for weights we care about single appearances

        return weights

    def correct_label_matching(self, train_dataset):
        print("Correcting class numbering")
        if self.mode == 'train':
            new_label_index_dict = dict()
            for label in self.data['labels']:
                if label not in new_label_index_dict:
                    new_label_index_dict[label] = len(new_label_index_dict.keys())
            self.data['numbered_labeled_index'] = new_label_index_dict
            self.data['numbered_labeled_index_inverse'] = {v:k for k,v in new_label_index_dict.items()}
            self.data['labels'] = [self.data['numbered_labeled_index'][label] for label in self.data['labels']]
        else:
            self.data['numbered_labeled_index'] = train_dataset.data['numbered_labeled_index']
            self.data['numbered_labeled_index_inverse'] = train_dataset.data['numbered_labeled_index_inverse']
            new_data_list = []
            new_labels_list = []
            for sample_id, label in enumerate(self.data['labels']):
                if label in self.data['numbered_labeled_index']:
                    new_labels_list.append(self.data['numbered_labeled_index'][label])
                    new_data_list.append(self.data['data'][sample_id])
            self.data['labels'] = new_labels_list
            self.data['data'] = new_data_list

    def set_gradient_emb(self, bool_val):
        self.require_grad = bool_val

    def __len__(self):
        return len(self.data['data'])

    def __getitem__(self, index):
        return {'data': torch.tensor(self.data['data'][index], requires_grad=False).float(),
                'label': torch.tensor(self.data['labels'][index], requires_grad=False).long()}

    @staticmethod
    def pad_tokens(list_of_tokens, max_length):
        return list(mit.padded(list_of_tokens, '[PAD]', max_length))
    
    def load_data(self, data_dir, labels_file_path, tokeniser, max_num_tokens, get_BERT_embeddings,
                  batch_size_for_BERT_embeddings, token_embeddings, doc_count_break_point, do_padding,
                  best_match_paragraph, top_k, train_dataset, device, get_word_count_emb, word_count_emb_max_words, add_CLS_SEP=True):
        if add_CLS_SEP:
            print("Using [CLS] and [SEP] tags")
            max_num_tokens_edit = -2
        else:
            max_num_tokens_edit = 0

        labels_dict = dict()
        labels_inverse_dict = dict()

        for label_entry in open(labels_file_path, "r").readlines():
            doc_id = label_entry.split()[0]
            label_s = label_entry.split()[1:]
            labels_dict[doc_id] = label_s
            for label in label_s:
                if label not in labels_inverse_dict:
                    labels_inverse_dict[label] = [doc_id]
                else:
                    labels_inverse_dict[label].append(doc_id)
        if self.mode == 'train':
            labels_index_dict = dict(zip(labels_inverse_dict.keys(), range(len(labels_inverse_dict.keys()))))
            labels_index_inverse = {v: k for k, v in labels_index_dict.items()}
        else:
            labels_index_dict = train_dataset.data['labels_index_dict']
            labels_index_inverse = train_dataset.data['labels_index_inverse']

        data_list = []
        labels_list = []

        if best_match_paragraph:
            n_no_matched = 0

        for doc_count, doc in enumerate(sorted(os.listdir(data_dir))):
            doc_path_id = os.path.join(data_dir, doc)
            try:
                line = open(doc_path_id, "r", encoding="latin-1").read()
                document_text = " ".join(line.split())
            except Exception as e:
                print(line)
                raise e
            tokens = tokeniser.tokenize(document_text)
            if not doc_path_id in labels_dict:
                raise Exception("{} not in dictionary of labels, constructed from file {}".format(doc_path_id, labels_file_path))

            label = labels_index_dict[labels_dict[doc_path_id][0]]

            if len(tokens) > max_num_tokens + max_num_tokens_edit:
                n = np.ceil(len(tokens)/(max_num_tokens + max_num_tokens_edit))
                tokens = np.array_split(tokens, n)
                for indx, t in enumerate(tokens):
                    if add_CLS_SEP:
                        tokens[indx] = ['[CLS]'] + list(t) + ['[SEP]']
                    else:
                        tokens[indx] = list(t)
            else:
                if add_CLS_SEP:
                    tokens = [['[CLS]'] + list(tokens) + ['[SEP]']]
                else:
                    tokens = [tokens]

            if best_match_paragraph:
                best_paragraph = None
                best_paragraph_label_match = None
                for label_id, lbl in enumerate(labels_dict[doc_path_id]):
                    lbl_token = lbl.split('-')[0]
                    for paragraph_id, paragraph in enumerate(tokens):
                        if lbl_token in paragraph:
                            best_paragraph = paragraph_id
                            best_paragraph_label_match = label_id
                            break
                    if best_paragraph is not None:
                        break
                if best_paragraph is None:
                    best_paragraph = 0
                    best_paragraph_label_match = 0
                    n_no_matched += 1
                tokens = [tokens[best_paragraph]]
                label = labels_index_dict[labels_dict[doc_path_id][best_paragraph_label_match]]

            for tokens_chunk in tokens:
                if do_padding:
                    tokens_chunk = self.pad_tokens(tokens_chunk, max_num_tokens)
                data_list.append(tokeniser.convert_tokens_to_ids(tokens_chunk)) # [1,512]
                labels_list.append(label)

            if not doc_count_break_point is None and doc_count >= doc_count_break_point:
                break

        if best_match_paragraph:
            print("Number of documents without a matching label: {}/{}".format(n_no_matched, len(os.listdir(data_dir))))

        if top_k > 0:
            if self.mode == 'train':
                list_top_k_cats = []
                for indx, (cat, docs) in enumerate(collections.OrderedDict(sorted(labels_inverse_dict.items(), key=lambda x: len(x[1]), reverse=True)).items()):
                    list_top_k_cats.append(cat)
                    if indx+1 >= top_k:
                        break
                print("Using the top {} categories "
                      "(cat, cat_number, size): {}".format(top_k, [(i, labels_index_dict[i],
                                                                    len(labels_inverse_dict[i])) for i in list_top_k_cats]))
            else:
                list_top_k_cats = train_dataset.data['list_top_k_cats']
                print(list_top_k_cats)
            data_list_new = []
            labels_list_new = []
            for paragrpah_indx, cat_lbl in enumerate(labels_list):
                cat = labels_index_inverse[cat_lbl]
                if cat in list_top_k_cats:
                    data_list_new.append(data_list[paragrpah_indx])
                    labels_list_new.append(labels_list[paragrpah_indx])
            data_list = data_list_new
            labels_list = labels_list_new
            del data_list_new, labels_list_new
        else:
            list_top_k_cats = None

        if get_BERT_embeddings:
            data_list_bert_emb = []
            print("Using BERT embeddginds from pre-trained model. Converting with batch size {}".format(batch_size_for_BERT_embeddings))
            bert_model = BertForSequenceClassification.from_pretrained('bert-base-uncased', output_hidden_states=True).to(device)
            bert_model.eval()
            with torch.no_grad():
                for x_indx in tqdm.tqdm(range(0, len(data_list), batch_size_for_BERT_embeddings)):
                    end_point = min(len(data_list), x_indx+batch_size_for_BERT_embeddings)
                    x = torch.tensor(data_list[x_indx:end_point], requires_grad=False).long()
                    # x_out_tokens, x_out = bert_model(x.to(device))
                    out = bert_model(x.to(device))
                    x_out = out[1][-1].mean(1)
                    assert (np.array([len(np.unique(a)) for a in x_out.numpy()]) > 2).all()
                    if token_embeddings:
                        x_out_tokens = out[1][-1]
                        data_list_bert_emb.extend(x_out_tokens[5].numpy())
                    else:
                        data_list_bert_emb.extend(x_out.numpy())
        elif get_word_count_emb:
            kerastokenizer = keraTokenizer(num_words=word_count_emb_max_words)
            data_list = kerastokenizer.sequences_to_matrix(data_list, mode='binary')

        return {'data': data_list_bert_emb if get_BERT_embeddings else data_list, 'labels': labels_list,
                'labels_dict': labels_dict, 'labels_inverse_dict': labels_inverse_dict,
                'labels_index_dict': labels_index_dict, 'labels_index_inverse': labels_index_inverse,
                'list_top_k_cats': list_top_k_cats}


def initialise_data(config, mode, train_dataset=None):
    allowed_modes = ['train', 'test']
    if not mode in allowed_modes:
        assert Exception("mode '{}' not reckognized. Allowed modes: {}".format(mode, allowed_modes))

    args = {'data_dir':config[mode+'_data_dir'], 'load_from_file': config[mode+'_load_from_file'],
            'save_to_file': config[mode+'_save_to_file'], 'mode': mode, 'train_dataset': train_dataset}

    dataset = reuters_dataset(config, args)
    return DataLoader(dataset, batch_size=config['batch_size'], shuffle=True), dataset

