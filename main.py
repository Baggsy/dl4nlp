import argparse
import time
import torch
import numpy as np
import torch.optim as optim
import torch.nn as nn
from pytorch_pretrained_bert import BertTokenizer
from dataset_loading import initialise_data
from utils import init_experiment, log_update_experiment_info, save_plots_param
from models import Simple, CNN, KimCNN, set_dropout_to_train, Simple_with_dropout, Simple_plus_with_dropout, LSTM
from sklearn.metrics import f1_score, accuracy_score


def initialize_model(config):
    ind_dim = config['bert_emb_dim'] if config['get_BERT_embeddings'] else config['word_count_emb_max_words']
    if config['model'] == 'simple':
        model = Simple(dimensions=ind_dim, num_classes=config['num_classes'])
    elif config['model'] == 'CNN':
        model = CNN(in_dim=ind_dim, conv_channels=config['conv_channels'], num_classes=config['num_classes'])
    elif config['model'] == 'KimCNN':
        model = KimCNN(in_dim=ind_dim, num_classes=config['num_classes'])
    elif config['model'] == 'simpleB':
        model = Simple_with_dropout(dimensions=ind_dim, num_classes=config['num_classes'],
                                    bayesian_dropout_prob=config['bayesian_dropout_prob'], do_drop_in_testing=config['bayesian_dropout'])
    elif config['model'] == 'simple_plus':
        model = Simple_plus_with_dropout(dimensions=ind_dim, num_classes=config['num_classes'],
                                         bayesian_dropout_prob=config['bayesian_dropout_prob'])
    elif config['model'] == 'LSTM':
        model = LSTM(dimensions=ind_dim, num_classes=config['num_classes'],
                                         bayesian_dropout_prob=config['bayesian_dropout_prob'])
    else:
        raise NotImplementedError

    optimizer = optim.Adam(model.parameters(), lr=config['lr'])
    return model.to(config['device']), optimizer


def perform_epoch(dataloader, model, optimiser, criterion, config, mode):
    batch_losses = []
    metrics_pred = []
    metrics_true = []
    epoch_avg_acc = 0
    if config['bayesian_dropout']:
        mean_var = []
        true_label_var = []
    if config['bayesian_dropout']:
        KL_loss = nn.KLDivLoss()
    for batch_id, batch in enumerate(dataloader):
        if mode == 'train':
            optimiser.zero_grad()

        data = batch['data'].to(config['device'])
        labels = batch['label'].to(config['device'])

        output = model(data)
        loss = criterion(output, labels)

        if config['bayesian_dropout']:
            temp_out = (torch.ones_like(output)*output).unsqueeze(0)
            for i in range(config['bayesian_dropout_T']-1):
                temp_out = torch.cat((temp_out, model(data).unsqueeze(0)), dim=0)
            batch_var = temp_out.var(dim=0)
            mean_var.append(batch_var.detach())
            true_label_var.extend([batch_var[id][lbl].detach().numpy() for id, lbl in enumerate(labels)])
            kl_target = temp_out.mean(dim=0).detach()
            kll = -KL_loss(output, kl_target)
            loss += kll
            if config['penalise_loss_with_std']:
                loss += torch.log(1+batch_var.mean())

        if mode == 'train':
            loss.backward()
            optimiser.step()

        batch_losses.append(loss.cpu().detach().numpy())

        temp = output.argmax(dim=1, keepdim=True).view_as(labels)
        correct = labels.eq(temp).sum().item()
        epoch_avg_acc += correct / config['batch_size']

        # Calculate metrics
        metrics_pred.extend(output.detach().max(1)[1].numpy())
        metrics_true.extend(labels.numpy())

    metrics = {'f1': f1_score(metrics_true, metrics_pred, average='weighted'),
                'acc': accuracy_score(metrics_true, metrics_pred)}

    if config['bayesian_dropout']:
        sum_var = 0.0
        sum_var_all = []
        count_var_all = 0
        count_var = 0
        for b in mean_var:
            sum_var += b.sum().numpy()
            sum_var_all.append(b.sum(dim=0).numpy())
            count_var_all += b.size(0)
            count_var += b.size(0)*b.size(1)
        metrics['mean_var'] = sum_var/count_var
        metrics['true_label_var'] = np.mean(true_label_var)
        metrics['all_labels_var'] = np.array(np.sum(sum_var_all, axis=0))/count_var_all
    return np.mean(batch_losses), metrics


def train(config):
    # Set random seed + Initialise device
    init_experiment(config)

    config['tokeniser'] = BertTokenizer.from_pretrained('bert-base-uncased')
    train_dataloader, train_dataset = initialise_data(config, mode='train')
    test_dataloader, test_dataset = initialise_data(config, mode='test', train_dataset=train_dataset)

    # Initialise model
    model, optimiser = initialize_model(config)
    criterion = nn.CrossEntropyLoss(torch.Tensor(train_dataset.get_class_weights()) if config['weighted_loss'] else None)

    # Iterate over epochs
    config['log'].info('[epoch/#epochs] | Train loss, Test loss | Train F1-score, Test F1-score | Train Acc, Test Acc')
    epoch_info = dict()
    epoch_info['max_score'] = -1
    epoch_info['max_score_epoch'] = 0
    epoch_info['max_score_epoch_info'] = None
    for epoch in range(1, config['epochs'] + 1):
        model.train()
        epoch_info['train_loss'], epoch_info['train_metrics'] = perform_epoch(train_dataloader, model, optimiser, criterion, config, mode='train')

        model.eval()
        if config['bayesian_dropout']:
            model.apply(set_dropout_to_train)
        with torch.no_grad():
            epoch_info['test_loss'], epoch_info['test_metrics'] = perform_epoch(test_dataloader, model, optimiser, criterion, config, mode='eval')

        log_update_experiment_info(config, epoch_info, epoch)

        if config['do_early_stopping'] and config['early_stopping'].step(epoch_info['train_metrics']['f1']):
            config['log'].info("\n --- Early stopping criterion reached --- ")
            break

    save_plots_param(config, epoch_info)


def main():
    parser = argparse.ArgumentParser()
    # Global
    parser.add_argument("--model", type=str, default="simple", choices=['CNN', 'LSTM', 'CapsNet', 'MLP', 'KimCNN', 'simple', 'simpleB', 'simple_plus'])
    parser.add_argument("--num_classes", type=int, default=10)
    parser.add_argument("--train_data_dir", type=str, default="training/")
    parser.add_argument("--test_data_dir", type=str, default="test/")
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--lr", type=float, default=1e-4)
    parser.add_argument("--epochs", type=int, default=10)
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--resume", type=str, help="e.g.: 'results/exp_1/best_model'")
    parser.add_argument("--log_filename", type=str, default='output.log')
    parser.add_argument("--results_dir", type=str, default='results')
    parser.add_argument("--save_model_to_file", type=str, default=None,
                        help='which file to save nn model to after training')
    parser.add_argument("--tb_log_file", type=str, default='tb_logs',
                        help='where to store tensorboard log')
    parser.add_argument("-no_weighted_loss", dest="weighted_loss", action="store_false")
    parser.add_argument('-no_bayesian_dropout', dest='bayesian_dropout', action='store_false')
    parser.add_argument('-penalise_loss_with_std', dest='penalise_loss_with_std', action='store_true')
    parser.add_argument("--bayesian_dropout_prob", type=float, default=0.5)
    parser.add_argument("--bayesian_dropout_T", type=int, default=6)

    # Dataloading specific
    parser.add_argument("--train_load_from_file", type=str)
    parser.add_argument("--train_save_to_file", type=str, help='e.g.: train_data')
    parser.add_argument("--test_load_from_file", type=str)
    parser.add_argument("--test_save_to_file", type=str, help='e.g.: test_data')
    parser.add_argument("--labels_file_path", type=str, default='cats.txt')
    parser.add_argument("--batch_size_for_BERT_embeddings", type=int, default=32)
    parser.add_argument("-no_BERT_embeddings", dest="get_BERT_embeddings", action="store_false")
    parser.add_argument("-token_embeddings", dest="token_embeddings", action="store_true")
    parser.add_argument("-dont_pad", dest="do_padding", action="store_false")
    parser.add_argument("-best_match_paragraph", dest="best_match_paragraph", action="store_true")
    parser.add_argument("--top_k", type=int, default=-1)
    parser.add_argument('--splits', type=int)
    parser.add_argument('--save_splits_dir', type=str, default='split_data')
    parser.add_argument('-correct_label_matching', dest='correct_label_matching', action='store_true')
    parser.add_argument('-get_word_count_emb', dest='get_word_count_emb', action='store_true')
    parser.add_argument('--word_count_emb_max_words', type=int, default=10000)
    parser.add_argument("--max_num_tokens", type=int, default=512)
    parser.add_argument("--bert_emb_dim", type=int, default=768)

    # Early stopping
    parser.add_argument("-do_early_stopping", dest='do_early_stopping', action='store_true')
    parser.add_argument("--early_stopping_mode", type=str, default='max')
    parser.add_argument("--early_stopping_delta", type=float, default=0.005)
    parser.add_argument("--early_stopping_patience", type=int, default=30)

    # CNN params
    parser.add_argument("--conv_channels", default=[1,10],
                        help="list of number of conv. filters per layer for cnn", nargs='+', type=int)
    # MLP params
    parser.add_argument("--mlp_linears", default=[200,100],
                        help="list of neurons per linear layer", nargs='+', type=int)
    # For debugging
    parser.add_argument("-d", dest="debug", action="store_true")
    parser.add_argument("-cpu", dest="cpu", action="store_true")
    parser.add_argument("--verbosity", type=int, default=1)
    parser.add_argument("-t", dest="temp", action="store_true")

    args = parser.parse_args()
    t0 = time.time()

    config = vars(args)
    train(config)

    print("\n-------------- Done in {0:.4f} [hours]---------------".format((time.time() - t0)/(60*60)))


if __name__ == '__main__':
    main()
