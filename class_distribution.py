import matplotlib.pyplot as plt

classes_train = [  94,  147,  136,  113,  257,  318,   66,  326,  337, 2848, 1608]
classes_test = [  20,   68,   52,   28,  109,  148,   16,  121,  101, 1086,  697]

plt.subplot(1, 2, 1)
plt.bar(range(11), classes_train)
plt.xticks(range(11))

plt.subplot(1, 2, 2)
plt.bar(range(11), classes_test)
plt.xticks(range(11))
plt.show()