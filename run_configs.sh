#!/bin/sh
#python -u main.py --model simple --train_load_from_file train_data_top_10 --test_load_from_file test_data_top_10 "${@}"
#python -u main.py --train_load_from_file train_data_top_10 --test_load_from_file test_data_top_10 "${@}"
#python -u main.py  "${@}"


# For keras embeddingds #python -u main.py --model simple  -no_BERT_embeddings --num_classes 83 -dont_pad -correct_label_matching
# CNN # python -u main.py -cpu -no_BERT_embeddings --num_classes 10 -dont_pad -correct_label_matching --top_k 10 --model CNN -get_word_count_emb --train_load_from_file train_data_word_count_emb --test_load_from_file test_data_word_count_emb
# 2 linear layers + MC dropout #  python -u main.py -cpu -no_BERT_embeddings --num_classes 10 -dont_pad -correct_label_matching --top_k 10 --model simple_plus -get_word_count_emb --train_load_from_file train_data_word_count_emb --test_load_from_file test_data_word_count_emb -penalise_loss_with_std

# for the lstm #python -u main.py -cpu -no_BERT_embeddings --num_classes 10 -dont_pad -correct_label_matching --top_k 10 --model LSTM -get_word_count_emb --train_load_from_file train_data_word_count_emb --test_load_from_file test_data_word_count_emb

#python -u main.py -cpu --num_classes 10 --top_k 10 --model simple_plus -no_BERT_embeddings -get_word_count_emb --train_load_from_file train_data_word_count_emb --test_load_from_file test_data_word_count_emb -penalise_loss_with_std

#python -u main.py -cpu --num_classes 10 --top_k 10 --model simple_plus -no_BERT_embeddings -get_word_count_emb --train_load_from_file train_data_word_count_emb --test_load_from_file test_data_word_count_emb

#python -u main.py -cpu --num_classes 10 -correct_label_matching --top_k 10 --epochs 0 --test_save_to_file test_data_top_10_BERT_emb --train_save_to_file train_data_top_10_BERT_emb -best_match_paragraph

python -u main.py -cpu --num_classes 10 --top_k 10 --model simple_plus --train_load_from_file train_data_top_10_BERT_emb --test_load_from_file test_data_top_10_BERT_emb -penalise_loss_with_std "${@}"