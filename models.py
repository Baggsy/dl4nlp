import torch
import torch.nn as nn
import numpy as np
from torch.nn import functional as F


class Simple(torch.nn.Module):
    def __init__(self, dimensions, num_classes):
        super(Simple, self).__init__()

        self.net = nn.Linear(dimensions, out_features=num_classes, bias=True)

    def forward(self, x):
        return self.net(x)


class Simple_with_dropout(Simple):
    def __init__(self, dimensions, num_classes, bayesian_dropout_prob, do_drop_in_testing):
        super(Simple_with_dropout, self).__init__(dimensions, num_classes)

        self.drop_prob = bayesian_dropout_prob
        self.do_drop_in_testing = do_drop_in_testing

    def forward(self, x):
        if self.do_drop_in_testing:
            return F.dropout(self.net(x), p=self.drop_prob, training=True)
        else:
            return F.dropout(self.net(x), p=self.drop_prob)


class Simple_plus_with_dropout(nn.Module):
    def __init__(self, dimensions, num_classes, bayesian_dropout_prob):
        super(Simple_plus_with_dropout, self).__init__()

        self.drop_prob = bayesian_dropout_prob

        self.net = nn.Sequential(
            nn.Linear(dimensions, out_features=256, bias=True),
            nn.ReLU(),
            nn.Dropout(bayesian_dropout_prob),
            nn.Linear(256, num_classes, bias=True)
        )

    def forward(self, x):
        return self.net(x)


class LSTM(nn.Module):
    def __init__(self, dimensions, num_classes, bayesian_dropout_prob):
        super(LSTM, self).__init__()

        self.drop_prob = bayesian_dropout_prob

        self.lstm = nn.LSTM(dimensions, hidden_size=256, bias=True, bidirectional=True)
        self.net = nn.Sequential(
            nn.ReLU(),
            nn.Dropout(bayesian_dropout_prob),
            nn.Linear(256*2, num_classes, bias=True)
        )

    def forward(self, x):
        x = x.unsqueeze(1)
        x = self.lstm(x)[0].squeeze(1)
        return self.net(x)


class CNN(torch.nn.Module):

    def __init__(self, in_dim, conv_channels, num_classes):
        super(CNN, self).__init__()

        self.in_dim = in_dim
        self.hw = [24,32] if self.in_dim == 768 else [100,100]
        self.conv_channels = []

        for i in range(len(conv_channels) - 1):
            in_channels = conv_channels[i]
            out_channels = conv_channels[i + 1]
            self.conv_channels.extend([
                nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=1, padding=(1,1), bias=True),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0),
                nn.ReLU()
            ])
        self.conv_channels = nn.Sequential(*self.conv_channels)

        # Calc output size after processing by conv channels
        for i in range(len(conv_channels) - 1):
            in_dim = self._output_size(in_dim, kernel_size=3, stride=1, padding=1)

        self.conv_out_size = ((in_dim) * conv_channels[-1])

        self.linears = nn.Sequential(nn.Linear(self.conv_out_size, out_features=50, bias=True),
                                     nn.ReLU(),
                                     nn.Dropout(0.5),
                                     nn.Linear(in_features=50, out_features=num_classes)
                                     )

    @staticmethod
    def _output_size(in_size, kernel_size, stride, padding):
        output = int((in_size - kernel_size + 2*padding) / stride) + 1
        return output//4

    def forward(self, x):
        # Pass through conv channels
        x = x.view(x.size(0), self.hw[0], self.hw[1]).unsqueeze(1)
        x = self.conv_channels(x)
        # Flatten
        x = x.view(x.size(0), -1)
        # Pass to linear layers
        x = self.linears(x)
        return x


class KimCNN(torch.nn.Module):
    def __init__(self, in_dim, num_classes):
        super(KimCNN, self).__init__()

        self.in_dim = in_dim
        self.conv_channels = 10

        self.conv1 = nn.Conv2d(in_channels=1, out_channels=self.conv_channels, kernel_size=3)
        self.conv2 = nn.Conv2d(in_channels=1, out_channels=self.conv_channels, kernel_size=4)
        self.conv3 = nn.Conv2d(in_channels=1, out_channels=self.conv_channels, kernel_size=5)

        self.fc = nn.Linear(140, num_classes)

    def _mot_pooling(self, x):
        # Max-over-time pooling
        # X is shape n,c,w
        x = F.max_pool2d(x, kernel_size=self.conv_channels)
        return x

    def forward(self, x):
        # Pass through conv channels
        x = x.view(x.size(0), 24, 32).unsqueeze(1)
        out1 = self.conv1(x)
        out2 = self.conv2(x)
        out3 = self.conv3(x)
        # Max over time pooling
        out1 = self._mot_pooling(out1)
        out2 = self._mot_pooling(out2)
        out3 = self._mot_pooling(out3)
        # Concatenate over channel dim
        out1 = out1.view(out1.size(0), out1.size(1), -1)
        out2 = out2.view(out2.size(0), out2.size(1), -1)
        out3 = out3.view(out3.size(0), out3.size(1), -1)
        out = torch.cat((out1, out2, out3), dim=2)
        # Flatten
        out = out.view(out.size(0), -1)
        # Pass to linear layers
        out = self.fc(out)

        return out


def set_dropout_to_train(m):
    if type(m) == nn.Dropout:
        m.train()

